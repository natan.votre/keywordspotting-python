# KeywordSpotting-Python

Neste trabalho é elaborado um _Keyword Spotting_ (KWS) utilizando a linguagem _python_ em _notebooks_.

O KWS desenvolvido utiliza o banco de dados contendo arquivos de fala **X** (_Speech Database_) e é composto por um extrator de _Mel Frequency Cepstrum Coefficients_ (MFCCs), uma _Convolutional Neural Network_ (CNN) e por um decisor.

São utilizadas as palavras "Sim Máquina", "Abre" e "Fecha" como _keywords_ do KWS proposto.

O trabalho é elaborado com o intuito de desenvolver o KWS em FPGA.

## Divisão

O trabalho é dividido em 6 etapas, separadas 6 _notebooks_: `SpeechDatabase`, `DataAugmentation`, `ExtractFeatures`, `CreateDataset`, `TrainingCNN` e `Evaluate`.

### Speech Database

Nesta etapa é realizada a formatação do _Speech Database_.

Através de uma tabela, são organizados os arquivos de fala para realizar posteriormente a expansão de dados (_Data Augmentation_), extração do conjunto de dados (_Dataset_) e Avaliação do KWS.

### Data Augmentation

Nesta etapa é realizada a expansão do banco de dados através da técnica **Data Augmentation**.
Esta técnica utiliza transformações do tipo _time shift_, _pitch shift_ e _time stretch_.

O _time shift_ é realizado no _**Extract Features**_, versões da mesma ocorrência de _keyword_ realizando pequenos deslocamentos no tempo.

Nesta etapa serão utilizadas as técnicas _pitch shift_ e _stretch_, gerando novos arquivos de áudio.

### Extract Features

Nesta etapa é realizada a extração de _features_ do _Speech Database_ + _Data Augmented_. É utilizado um extrator de MFCCs para elaborar um _dataset_ a partir dos arquivos de fala.
São utilizados os ponteiros gerados no _notebook_ **SpeechDatabase.ipynb** para realizar a extração, sendo que o centro dos _frames_ processados é a posição do ponteiro.

Nesta etapa é utilizado o _time shift_ como técnica do _Data Augmentation_, gerando variação temporal dos ponteiros.

Os principais parâmetros utilizados na extração estão situados no arquivo python __envparams.py__.

### Create Dataset

Nesta etapa são obtidos os _datasets_ de treinamento, teste e validação, a partir das _features_ coletadas no **ExtractFeatures.ipynb**.

São recolhidas todas as _features_ extraídas no _notebook_ anterior e realizado um _shuffling_ (embalhamento), fazendo com que os _batchs_ de treinamento da CNN sejam o mais aleatórios possível.

### Training CNN

Nesta etapa é gerado um modelo de rede neural do tipo convolucional (CNN).

Também é realizado o treinamento da CNN utilizando os _datasets_ obtidos no _notebook_ **CreateDataset.ipynb**.

### Evaluate

Nesta etapa são realizadas as avaliações do KWS proposto. 

É realizado a execução do KWS nos conjuntos de áudio, detectando as _keywords_ e definindo _thresholds_ para as _keywords_.

As avaliações consistem na acurácia das _keywords_ e na análise dos falsos alarmes.
